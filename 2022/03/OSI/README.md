# README

The aim was to show how the Open Source Initiative (OSI) board is elected,
composed and the remaining terms of those currently serving. The context was the
upcoming [OSI Board Elections
2022](https://opensource.org/blog/osi-board-elections-2022).

---

UPDATE MAY 2022: Create your own visualisations online with [Figular](https://figular.com/).
The work here can be quickly replicated with the new figure called [board
election](https://figular.com/tryit/case/boardelection/). Read more in the announcement
here: [a new Figure for the OSI Board
Election](https://figular.com/post/20220511172059/a-new-figure-for-the-osi-board-election/)

---

The final results are below and were crossposted on social media:

- Pixelfed.social:
  - [Board Seats and Terms](https://pixelfed.social/i/web/post/407209625504924157)
  - [Election Candidates](https://pixelfed.social/i/web/post/407915636654267917)
  - [Election Results](https://pixelfed.social/i/web/post/411589357460518345)
- Fosstodon (a Mastodon instance):
  - [Election Candidates](https://fosstodon.org/web/@fosstian/107938727325734731)
  - [Election Results](https://fosstodon.org/web/@fosstian/107995951681523535)
- Twitter:
  - [Board Seats and Terms](https://twitter.com/MrCrufts/status/1501595680728616968)
  - [Election Candidates](https://twitter.com/MrCrufts/status/1502298446837030913)
  - [Election Results](https://twitter.com/MrCrufts/status/1505973728676569093)

Thanks to the following tools for making it possible:

- [LibreOffice](https://www.libreoffice.org/)
- [Inkscape](https://inkscape.org/)
- [Asymptote](https://asymptote.sourceforge.io/)
- [Gnuplot](http://gnuplot.info/)

...and thanks to the [OSI](https://opensource.org/) for their transparency in
publishing the information that these visuals are based on.

![OSI Board - Seats and Terms. A diagram of the ten OSI board seats grouped by
those elected from individuals, affiliates and those board-appointed.](OSI.png)

A second revision was added once candidates were confirmed:

![OSI Board - Election 2022. A diagram showing the nine candidates for the two individual
directorships and the eight candidates for the two affiliate directorships.](OSI_rev2.png)

And a final revision for the election results:

![OSI Board Election 2022 Results: two bar charts show the results for the
individual and affiliate elections.](OSI_rev3.png)

## Future Improvement

- It would be great to generate the org chart inside LibreOffice itself.
- If Figular would support hooks/adding custom code at the cmdline then we
  could just run `fig` directly. Allowing pre-processing of data and
  a post-process where we can add more titles etc would have saved time.
- I had to hard-code line breaks for some names which didn't break nicely. This
  probably comes down to my own lack of knowledge on LaTeX.
- Likewise a manual line break was needed for Jean-Brunel's name in rev2 so that
  the circle of candidates was not too big.
- It would have been nice to click on candidates to be taking to their
  candidate pages. However social media does not support attaching SVG/PDF
  formats to allow this.

## Process

I started from the article [OSI Board of
Directors](https://opensource.org/board) and pulled this into a LibreOffice
spreadsheet included in this repo [here](OSI.ods). From this information we
could calculate each board persons' term served and also normalise the names
(some where last name, first).

I could already see the data was not completely up-to-date as one person's term
had expired. Looking through recent OSI news gave the answer. The
latest [board meeting minutes](https://wiki.opensource.org/bin/Main/OSI%20Board%20of%20Directors/Board%20minutes/2022/2022-01-21/)
explained what had happened and a more recent post indicated some roles had
changed as the election approaches in [Meet OSI’s new board member, Chair and
Vice Chair](https://opensource.org/meet-osis-new-board-member-chair-and-vice-chair).

It was also clear four directors had shorter terms than might be expected.
This turned out to be due to a necessary re-run of 2021's election: [OSI’s 2021
Board Election is Concluded](https://opensource.org/2021ElectionConcluded).

As the data was only a few lines I made an extra column on the end that put it
all into a format Figular could understand and simple pasted this to the
cmdline. I then wrote some code to override how Figular renders each person in
its org chart to work in a pie chart of the term served (see [draw.asy](draw.asy)).

### Rev 2

A second revision was made to show the candidates once confirmed: [Meet OSI's
2022 candidates for Board of Directors | Open Source Initiative | Guaranteeing
the 'our' in
source...](https://opensource.org/blog/meet-osis-2022-candidates-for-board-of-directors).

Each list of candidates were put through Figular's 'circle' figure and then
arranged on the existing diagram with Inkscape.

### Rev 3

Once the [Election
Results](https://opensource.org/blog/2022-osi-board-election-results) were
available I just had to plug them into a Gnuplot I had already prepared (below).
Figular does not yet support plotting - a major gap - but this exercise
provided the experience to see what we'll need in future and if Gnuplot could
help.

The election uses an STV system so candidates' final vote tallies were taken
from their last round. In the end it was easiest to copy/paste the results into
vim, a bit of text mangling then into LibreOffice calc so see it all clearly.

### Prerequisites

Install (my own project) [figular](https://pypi.org/project/figular/) - `pip
install figular`. Note it has its own dependencies.

### Usage

There's an assumption Figular is installed in the user's site-packages below in
order to locate it.

```bash
asy -autoimport $(python -m site --user-site)/figular/org/orgchart.asy \
  draw.asy -f svg -o "osi".svg << EOF
OSI Board
Josh Simmons|Former Chair,96,23 of 24 month term served|OSI Board
Pamela Chestek|Director,97,35 of 36 month term served|OSI Board
Megan Byrd-Sanicki|Former Vice Chair,96,23 of 24 month term served|OSI Board
Italo Vignoli|Director,64,23 of 36 month term served|OSI Board
Aeva Black|Assistant Secretary,35,7 of 20 month term served|OSI Board
Hong Phuc Dang|Director,22,7 of 32 month term served|OSI Board
Catharina Maracke|Chair,35,7 of 20 month term served|OSI Board
Thierry Carrez|Vice Chair,22,7 of 32 month term served|OSI Board
Justin Colannino|Director,8,2 of 24 month term served|OSI Board
Tracy Hinds|Director,20,2 of 10 month term served|OSI Board
EOF
```

This gets the basic org chart (below) and then the rest of the titles were added
in Inkscape to give the final result.

![The ten OSI Board seats and roles](figular.svg)

For revision 2 the lists of candidates were submitted to the circle figure. This
was so trivial it was done via the web [Circle |
Figular](https://figular.com/tryit/concept/circle/) and the resulting SVGs
downloaded. Here's the individual candidates:

![The list of individual candidates arranged in a circle](IndCandidates.svg)

Here's the affiliate candidates:

![The list of affiliate candidates arranged in a circle](AffCandidates.svg)

For revision 3 two bar charts were added via gnuplot:

```gnuplot
# Choose a table of results
#############################################################

# Affiliate results
# $Data <<EOD
# "Benito Gonzalez" 1
# "Marco A. Gutierrez" 1
# "George DeMet" 3
# "Gael Blondelle" 3.58
# "Lior Kaplan " 4.25
# "Matt Jarvis" 7.42
# "Pamela Chestek" 11
# "Carlo Piana" 12.6
# EOD

# Individual results
$Data <<EOD
"Tetsuya Kitahata" 3
"Jean-Brunel Webb-Benjamin" 7
"Kevin P. Fleming" 14
"Rossella Sblendido" 14
"Jim Hall   " 19
"Myrle Krantz" 24
"Hilary Richardson" 42
"Josh Berkus" 59
"Amanda Brock " 63
EOD

# Setup stats, terminal and vars
#############################################################

stats $Data using 2 nooutput
set term post eps color background "0xe6e6e6" enhanced \
              size 5.04, STATS_records*.65
set output "plot.eps"
totalsoffset = 5
xticstep = floor((STATS_max * 0.96) / 3)

# Set styles etc
#############################################################

# These will distort with a lot more/less data
set label "VOTES" at first 0, STATS_records+0.7 font "Helvetica, 52.77"
set label "Total" at first STATS_max, STATS_records+0.7 right offset totalsoffset font "AvantGarge, 15.71"
set label "Votes" at first STATS_max, STATS_records+0.4 right offset totalsoffset font "AvantGarge, 15.71"
set tmargin at screen STATS_records/(STATS_records+2.0)
set lmargin 5
set rmargin (totalsoffset + 2)
set bmargin 5

set style fill solid 1
# offset here is character so font dependent which suits us
set ytics nomirror left textcolor rgbcolor "0x333333" offset 2.5 scale 0 font "AvantGarde, 15.71"
# Scale 0 removes the ticks
# We explicitly set start and end so we don't get the first/last tics and the
# grid follows this
set xtics xticstep, xticstep, 200 out scale 0 textcolor rgbcolor "0x333333" font "AvantGarde, 15.71" offset 0, -1
set x2tics xticstep, xticstep, 200 out scale 0 textcolor rgbcolor "0x333333" font "AvantGarde, 15.71" offset 0, 1
# [make xtics stay on top of every plot](https://groups.google.com/g/comp.graphics.apps.gnuplot/c/mH7XowoujKk)
set tics front
set grid front
set grid noytics xtics
unset border

# Plot
#############################################################
#
# Use two boxxy plots to get horizontal bar graphs
# To get the top two bars in dark green and all else in light green we first
# draw everyone in dark green, then draw the data again up to last two in light
# green.
# Finally use a labels plot to show the totals at the end of each bar
plot $Data using (0):(column(0)):(0):2:(column(0)-0.4):(column(0)+0.4):(0x3ca638):ytic(1) with boxxy notitle lc rgbcolor variable, \
     $Data every ::::STATS_records-3 using (0):(column(0)):(0):2:(column(0)-0.4):(column(0)+0.4):(0x9dd29b):ytic(1) with boxxy notitle lc rgbcolor variable, \
     $Data using (STATS_max):(column(0)):2 with labels font "AvantGarde, 15.71" right offset totalsoffset notitle

set output
```

The output of this for the individual data gives:

![plot_individual.eps](plot_individual.png)

And for the affiliate data:

![plot_affiliate.eps](plot_affiliate.png)
