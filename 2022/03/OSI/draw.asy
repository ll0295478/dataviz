picture dopie(real scaling, int percent, pen ourwhite) {
  picture result;
  real scalingadjust = 0.55;
  if(percent < 100) {
    path pie = (0, 0) -- arc((0,0), 1, 90, 90-360*(percent/100), direction=CW) -- cycle;
    fill(result, scale(scaling*scalingadjust)*pie, ourwhite);
    draw(result,scale(scaling*scalingadjust)*unitcircle, ourwhite);
  } else {
    fill(result,scale(scaling*scalingadjust)*unitcircle, ourwhite);
  }
  return result;
}

picture mydrawcard(string title, string subtitle) {
  picture pic;
  int fontsize = 12;
  real corner_dia = 3 * fontsize ;
  real card_height = 2.5 * corner_dia ;
  real card_width = 5 * corner_dia;
  pen titlepen = fontsize(fontsize) + AvantGarde("b");
  pen subtitlepen = fontsize(9) + Helvetica();
  pen ourwhite = rgb(0.9, 0.9, 0.9);
  pen ourblack = gray(0.2);

  // Box
  fill(pic,
       (-card_width/2,-card_height/2 + corner_dia/2)--
       (-card_width/2,card_height/2 - corner_dia/2){up}..
       (-card_width/2+corner_dia/2,card_height/2)--
       (card_width/2-corner_dia/2,card_height/2){right}..
       (card_width/2,card_height/2 - corner_dia/2)--
       (card_width/2,-card_height/2 + corner_dia/2){down}..
       (card_width/2-corner_dia/2,-card_height/2)--
       (-card_width/2+corner_dia/2,-card_height/2){left}..
       cycle,
       ourblack);

  // Title
  // As we are using align=N we add the equivalent of baseline() to the END of
  // our title to ensure its final line is always the typeface's full height
  // thus allowing us to position it the same regardless of the presence of
  // descenders
  string tex_strut = "\vphantom{\strut}"; 

  // Urgh: force a linebreak where we clearly need one
  if(title == "Megan Byrd-Sanicki") {
    title = "Megan\\Byrd-Sanicki";
  }
  if(title == "Hong Phuc Dang") {
    title = "Hong\\Phuc Dang";
  }
  string minipage_spec = title + tex_strut;
  // This is a rule of thumb that works for us
  real descenderadjust = fontsize/2;
  real titleoffset = .2*corner_dia;
  label(pic, minipage(minipage_spec, width=2.5*corner_dia),
       (corner_dia*3/4, titleoffset - descenderadjust), N, titlepen+ourwhite);

  // Big underline
  draw(pic, (-corner_dia/2,0)--(2*corner_dia,0), linewidth(1)+ourwhite);

  // Subtitle
  // Similar to title but add the baseline strut to the start as align=S.
  string role = subtitle;
  string[] info = split(subtitle, ",");
  if(info.length == 3) {
    role = info[0];
    add(pic, dopie(corner_dia, (int)info[1], ourwhite), (-1.5*corner_dia, titleoffset + fontsize*3/4));
    minipage_spec = "\centering{" + baseline(info[2]) + "}" ;
    label(pic, minipage(minipage_spec, width=1.75*corner_dia),(-1.5*corner_dia,-titleoffset),
          S, subtitlepen+ourwhite);
  }

  minipage_spec = baseline(role);
  label(pic, minipage(minipage_spec, width=2.5*corner_dia),(corner_dia*3/4,-titleoffset),
        S, subtitlepen+ourwhite);

  return pic;
}

drawcard =  mydrawcard;
run(currentpicture, stdin);
