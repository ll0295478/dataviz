picture mydrawcard(string title, string subtitle) {
  picture pic;
  int fontsize = 12;
  real corner_dia = 3 * fontsize ;
  real card_height = 3.5 * corner_dia ;
  real card_width = 5 * corner_dia;
  pen titlepen = fontsize(fontsize) + AvantGarde("b");
  pen subtitlepen = fontsize(9) + Helvetica();
  pen ourwhite = rgb(0.9, 0.9, 0.9);
  pen ourblack = gray(0.2);

  // Box
  fill(pic,
       (-card_width/2,-card_height/2 + corner_dia/2)--
       (-card_width/2,card_height/2 - corner_dia/2){up}..
       (-card_width/2+corner_dia/2,card_height/2)--
       (card_width/2-corner_dia/2,card_height/2){right}..
       (card_width/2,card_height/2 - corner_dia/2)--
       (card_width/2,-card_height/2 + corner_dia/2){down}..
       (card_width/2-corner_dia/2,-card_height/2)--
       (-card_width/2+corner_dia/2,-card_height/2){left}..
       cycle,
       ourblack);

  // Title
  // As we are using align=N we add the equivalent of baseline() to the END of
  // our title to ensure its final line is always the typeface's full height
  // thus allowing us to position it the same regardless of the presence of
  // descenders
  string tex_strut = "\vphantom{\strut}"; 
  string minipage_spec = "\centering{" + title + tex_strut + "}";
  // This is a rule of thumb that works for us
  real descenderadjust = fontsize/2;
  real titleoffset = .2*corner_dia;
  label(pic, minipage(minipage_spec, width=4*corner_dia),
       (0, 0), NoAlign, titlepen+ourwhite);

  return pic;
}

drawcard =  mydrawcard;
run(currentpicture, stdin);
